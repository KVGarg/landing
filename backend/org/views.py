import json

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_common.http import HttpResponse
from rest_framework import generics
from .serializers import FeedbackSerializer, CalendarEventsSerializer
from org.models import (
    Contributor, Feedback, CalendarEvent,
    Team, InactiveIssue, UnassignedIssue
)
from org.newcomers import active_newcomers
from org.serializers import OrgTeamsSerializer
from igitt_django.models import IGittIssue, IGittMergeRequest
from brake.decorators import ratelimit


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(['GET'])
def contrib(request):
    response = Contributor.objects.all()
    data = [{'name': item.name,
             'issues': item.issues_opened,
             'contributions': item.num_commits,
             'bio': item.bio,
             'login': item.login,
             'reviews': item.reviews,
             'public_repos': item.public_repos,
             'public_gists': item.public_gists,
             'followers': item.followers,
             'teams': [team.name for team in item.teams.all()],
             'location': item.location
             } for item in response]
    return HttpResponse(json.dumps(data), content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def issues(request, hoster):
    issues = IGittIssue.objects.all()
    issues = [issue.data for issue in issues
              if issue.repo.hoster == hoster]
    # param default=str is used to dump the datetime object into string.
    return HttpResponse(
        json.dumps(issues, indent=4, default=str),
        content_type='application/json',)


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def merge_requests(request, hoster):
    mrs = IGittMergeRequest.objects.all()
    mrs = [mr.data for mr in mrs
           if mr.repo.hoster == hoster]
    # param default=str is used to dump the datetime object into string.
    return HttpResponse(
        json.dumps(mrs, indent=4, default=str),
        content_type='application/json',)


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def newcomers_active(request):
    newcomers_list = active_newcomers()
    active_newcomers_list = []
    for newcomer in newcomers_list:
        active_newcomers_list.append({'username': newcomer})
    return HttpResponse(
        json.dumps(active_newcomers_list),
        content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def get_inactive_issues(request):
    issues = InactiveIssue.objects.all()
    issues_list = []
    for issue in issues:
        issues_list.append(issue.issue_id.data)
    return HttpResponse(
        json.dumps(issues_list),
        content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def get_unassigned_issues(request):
    issues = UnassignedIssue.objects.all()
    issues_list = []
    for issue in issues:
        issues_list.append(issue.issue_id.data)
    return HttpResponse(
        json.dumps(issues_list),
        content_type='application/json')


class FeedbackListView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer


class CalendarEventsListView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = CalendarEvent.objects.all()
    serializer_class = CalendarEventsSerializer


class OrganizationTeams(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = Team.objects.all()
    serializer_class = OrgTeamsSerializer
