from django.db import models
import json

from igitt_django.models import IGittIssue


class Repo(models.Model):
    name = models.TextField(default=None, primary_key=True)
    repo_data = models.TextField(default=None, null=True)
    contributors_data = models.TextField(default=None, null=True)

    def repo_data_to_json(self, repo_data):
        self.repo_data = json.dumps(repo_data)

    def repo_data_from_json(self):
        return json.loads(self.repo_data)

    def contributors_data_to_json(self, repo_data):
        self.contributors_data = json.dumps(repo_data)

    def contributors_data_from_json(self):
        return json.loads(self.contributors_data)


class Team(models.Model):
    name = models.CharField(max_length=200, default=None)
    description = models.TextField(max_length=500, default=None, null=True)
    members_count = models.PositiveSmallIntegerField(default=0)
    increased_count = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.name


class Contributor(models.Model):
    login = models.TextField(default=None, primary_key=True)
    name = models.TextField(default=None, null=True)
    bio = models.TextField(default=None, null=True)
    num_commits = models.IntegerField(default=None, null=True)
    public_repos = models.IntegerField(default=None, null=True)
    public_gists = models.IntegerField(default=None, null=True)
    followers = models.IntegerField(default=None, null=True)
    reviews = models.IntegerField(default=None, null=True)
    issues_opened = models.IntegerField(default=None, null=True)
    location = models.TextField(default=None, null=True)
    teams = models.ManyToManyField(Team)


class Feedback(models.Model):
    username = models.CharField(max_length=50, default=None, null=True,
                                verbose_name='GitHub Username')
    feedback = models.TextField(max_length=1000, verbose_name='Feedback')
    date = models.DateTimeField(verbose_name='Date')
    experience = models.CharField(
        max_length=20, verbose_name='Feeling about organization?'
    )

    def __str__(self):
        return self.feedback[:100]


class CalendarEvent(models.Model):
    user = models.ForeignKey(Contributor, on_delete=models.CASCADE)
    title = models.TextField(max_length=300, verbose_name='Event title')
    description = models.TextField(
        max_length=1000, null=True, verbose_name='Event description'
    )
    start_date_time = models.DateTimeField(
        verbose_name='Event occurrence date and time(in UTC)'
    )
    end_date_time = models.DateTimeField(
        null=True, verbose_name='Event end date and time(in UTC)'
    )

    def __str__(self):
        return self.title


class InactiveIssue(models.Model):
    issue_id = models.ForeignKey(IGittIssue, on_delete=models.CASCADE)


class UnassignedIssue(models.Model):
    issue_id = models.ForeignKey(IGittIssue, on_delete=models.CASCADE)
