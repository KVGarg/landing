# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-08-01 20:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('org', '0006_auto_20190801_2043'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='description',
            field=models.TextField(default=None, max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='team',
            name='increased_count',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='members_count',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
