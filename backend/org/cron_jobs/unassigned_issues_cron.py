from functools import lru_cache

from igitt_django.models import (
    IGittMergeRequest,
    IGittRepository,
    IGittIssue
)

from org.models import UnassignedIssue


@lru_cache(maxsize=32)
def filter_issues():
    mrs = IGittMergeRequest.objects.all()
    for mr in mrs:
        data = mr.data
        if(data['state'] == 'open'
           and data['commit_status'][0]):
            for issue in data['closes_issues']:
                repo_name = issue['repository']
                hoster = issue['hoster']
                igitt_repo = IGittRepository.objects.get(full_name=repo_name,
                                                         hoster=hoster)
                igitt_issue = IGittIssue.objects.get(number=issue['number'],
                                                     repo_id=igitt_repo.id)
                igitt_issue_data = igitt_issue.data
                assignees = igitt_issue_data['assignees']
                if data['author'] not in assignees:
                    UnassignedIssue.objects.get_or_create(issue_id=igitt_issue)
