import time
from functools import lru_cache

from datetime import date
from datetime import datetime
from igitt_django.models import IGittIssue

from org.models import InactiveIssue


def filter_issues(issues):
    for issue in issues:
        data = issue.data
        if len(data['assignees']) > 0:
            issue_updation = datetime.strptime(data['updated_at'],
                                               '%Y-%m-%dT%H:%M:%S')
            assigned_duration = (date.fromtimestamp(time.time())
                                 - issue_updation.date()).days
            if assigned_duration >= 60:
                number = data['number']
                repo_id = data['repo_id']
                if IGittIssue.objects.filter(number=number,
                                             repo_id=repo_id).exists():
                    i = IGittIssue.objects.get(number=number,
                                               repo_id=repo_id)
                    InactiveIssue.objects.get_or_create(issue_id=i)


@lru_cache(maxsize=32)
def scrapper():
    issues = IGittIssue.objects.all()
    issues_list = []
    for issue in issues:
        issue_data = issue.data
        if 'status/blocked' not in issue_data['labels']:
            if(issue_data['state'] == 'open'
                    and len(issue_data['mrs_closed_by']) == 0):
                issues_list.append(issue)
    filter_issues(issues_list)
