import datetime
from time import sleep
from collections import defaultdict

import urllib
import requests

from IGitt.GitHub import GitHub
from IGitt.GitHub.GitHub import GitHub as GitHubMain
from IGitt.Interfaces import get_response

from org.models import Contributor, Team
from org.git import (
    get_org,
    GITHUB_TOKEN,
    get_igitt_org,
    valid_github_username,
    )


def fetch_org():
    data = defaultdict(lambda: defaultdict(int, {'name': '', 'bio': '',
                                                 'teams': []}))
    github_org = get_igitt_org('github')
    repos = github_org.repositories
    for repo in repos:
        commit_data = get_commits(repo, data)
        data = get_issues(repo, commit_data)

    session = requests.Session()
    for contributor in data:
        query_param = 'commenter:' + contributor + ' is:pr user:coala'
        encoded_query_param = urllib.parse.quote(query_param)
        search_url = GitHubMain.absolute_url(
            '/search/issues?q=' + encoded_query_param)
        response = get_response(session.get,
                                search_url,
                                GITHUB_TOKEN.auth)
        r_json, other_data = response
        data[contributor]['reviews'] = r_json.get('total_count', 0)
        rate_limit_url = GitHubMain.absolute_url('/rate_limit')
        rate = GitHub.get(GITHUB_TOKEN, rate_limit_url)
        if rate['resources']['search']['remaining'] == 1:
            sleep(60)

    for contributor in data:
        url = GitHubMain.absolute_url('/users/' + contributor)
        user = GitHub.get(GITHUB_TOKEN, url)
        if user['bio']:
            data[contributor]['bio'] = user['bio']
        if user['name']:
            data[contributor]['name'] = user['name']
        data[contributor]['public_repos'] = user['public_repos']
        data[contributor]['public_gists'] = user['public_gists']
        data[contributor]['followers'] = user['followers']

    org = get_org()
    teams = org.get_teams()
    for team in teams:
        t, created = Team.objects.get_or_create(name=team.name)
        t.description = team.description
        if created:
            t.members_count = team.members_count
        else:
            if datetime.datetime.now().timetuple().tm_yday % 7 == 0:
                t.increased_count = team.members_count - t.members_count
                t.members_count = team.members_count
        t.save()

        contributors = team.get_members()
        for contributor in contributors:
            contributor = contributor.login
            data[contributor]['teams'].append(team.name)

    # Saving dict in database
    teams = Team.objects.all()
    for contributor in data:
        if Contributor.objects.filter(login=contributor).exists():
            c = Contributor.objects.get(login=contributor)
        else:
            c = Contributor()
            c.login = contributor
        c.name = data[contributor]['name']
        c.bio = data[contributor]['bio']
        c.num_commits = data[contributor]['contributions']
        c.issues_opened = data[contributor]['issues']
        c.reviews = data[contributor]['reviews']
        c.public_repos = data[contributor]['public_repos']
        c.public_gists = data[contributor]['public_gists']
        c.followers = data[contributor]['followers']
        for team in teams:
            if team.name in data[contributor]['teams']:
                c.teams.add(team)
        c.save()


def get_commits(repo, data, exclude_invalid_username=True):
    """
    Get number of commits for each contributor(with valid usernames)
    to coala org. We are using a hard coded function to check the
    validity of the usernames as IGitt doesn't support it yet.
    Issue link: https://gitlab.com/gitmate/open-source/IGitt/issues/138

    :param repo: IGitt repository object
    :param data: a default dict
    :param exclude_invalid_username: to exclude invalid usernames
    :return: a default dict, contaning contributions
             for each contributor to coala
    """
    url = GitHubMain.absolute_url('/repos/' +
                                  repo.full_name + '/contributors')
    response = GitHub.get(GITHUB_TOKEN, url)
    for contributor in response:
        if not valid_github_username(contributor['login']):
            continue
        data[contributor['login']]['contributions'] +=\
            contributor['contributions']
    return data


def get_issues(repo, data):
    issues = repo.filter_issues(state='all')
    for issue in issues:
        data[issue.author.username]['issues'] += 1
    return data
