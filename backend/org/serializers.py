from rest_framework import serializers

from org.models import (
    CalendarEvent,
    Feedback,
    Team
)


class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Feedback
        fields = '__all__'


class CalendarEventsSerializer(serializers.ModelSerializer):

    class Meta:
        model = CalendarEvent
        fields = '__all__'

    def validate_user(self, username):
        user_teams = username.teams.all().count()
        if user_teams == 1:
            raise serializers.ValidationError(
                "{} isn't a member of developer team.".format(username.login)
            )
        return username

    def validate_title(self, value):
        if len(value.strip()) < 10:
            raise serializers.ValidationError(
                'The calendar event title should be more than 10 characters.'
            )
        return value.strip()

    def validate_description(self, value):
        return value.strip()


class OrgTeamsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = '__all__'
