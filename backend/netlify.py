import os
import requests
import logging


class Netlify:

    def __init__(self):
        self.base_url = 'https://api.netlify.com/api/v1/'
        self.app_id = os.environ.get('NETLIFY_APP_ID')
        self.access_token = os.environ.get('NETLIFY_ACCESS_TOKEN')
        self.headers = {
            'Authorization': 'Bearer {}'.format(self.access_token)
        }

    def get_form_id_for(self, form_name):
        """
        Fetch the FormID for a given netlify form name
        :param form_name: The name of the netlify form
        :return: Form ID, for the given form name
        """
        forms_api_endpoint = '{base_url}sites/{site_id}/forms'.format(
            base_url=self.base_url, site_id=self.app_id)

        forms_list = requests.get(url=forms_api_endpoint, headers=self.headers)
        for form in forms_list.json():
            if form['name'] == form_name:
                return forms_api_endpoint, form['id']
        raise ValueError('Form with name {} not found.'.format(form_name))

    def get_submissions(self, form_name):
        """
        Get all the form submissions for a given netlify form name
        :param form_name: The name of the netlify form
        :return: A JSON response containing items as the form submission
        made by user
        """
        forms_endpoint, form_id = self.get_form_id_for(form_name)

        form_endpoint = '{forms_endpoint}/{form_id}/submissions'.format(
            forms_endpoint=forms_endpoint, form_id=form_id)

        form_submissons = requests.get(form_endpoint, headers=self.headers)
        return form_submissons.json()

    def delete_submission(self, id):
        """
        Delete a given form submission with a given submission ID
        :param id: The form submission/entry ID of the user
        """
        delete_endpoint = '{base_url}submissions/{submission_id}'.format(
            base_url=self.base_url, submission_id=id)

        request_to_delete = requests.delete(delete_endpoint,
                                            headers=self.headers)
        if request_to_delete.status_code != 204:
            logging.error('Couldn\'t delete the submission {}'.format(id))
