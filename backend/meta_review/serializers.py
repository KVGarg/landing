from rest_framework import serializers

from .models import Participant


class MetaReviewerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Participant
        fields = '__all__'
