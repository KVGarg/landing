from django.conf.urls import url

from .views import MetaReviewersList

urlpatterns = [
    url(r'^$', MetaReviewersList.as_view()),
]
