from django.db.models import Q
from rest_framework import generics

from .models import Participant
from .serializers import MetaReviewerSerializer


class MetaReviewersList(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    serializer_class = MetaReviewerSerializer

    def get_queryset(self):
        return Participant.objects.all().exclude(
            Q(pos_in=0),
            Q(neg_in=0),
            Q(pos_out=0),
            Q(neg_out=0),
            Q(offset=0)
        )
