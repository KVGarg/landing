from django.apps import AppConfig


class OsformsConfig(AppConfig):
    name = 'osforms'
