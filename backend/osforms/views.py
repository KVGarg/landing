from rest_framework import generics
from osforms.models import OSFormRecord
from osforms.serializers import OSFormRecordsSerializer


class OSFormsListView(generics.ListAPIView):
    permission_classes = []
    authentication_classes = []
    queryset = OSFormRecord.objects.all()
    serializer_class = OSFormRecordsSerializer
