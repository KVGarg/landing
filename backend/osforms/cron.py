from functools import lru_cache
from IGitt.GitHub import GitHub
from IGitt.GitHub.GitHubUser import GitHubUser
import logging
import os

from org.git import GITHUB_TOKEN
from org.models import Contributor
from coala_web.settings import NETLIFY
from osforms.serializers import OSFormRecordsSerializer
from utilities import send_email
from coala_web.settings import ORG_NAME


@lru_cache(maxsize=32)
def fetch_osforms():
    form_name = os.environ.get('OSFORMS_NETLIFY_FORM_NAME')

    if not form_name:
        logging.error('OSForms netlify form name not provided! Please set'
                      'environment variable OSFORMS_NETLIFY_FORM_NAME.')
    else:
        submissions = NETLIFY.get_submissions(form_name)
        for entry in submissions:
            user_ = entry['data']
            data = {
                'user': user_['user'],
                'title': user_['title'],
                'description': user_['description'],
                'url': user_['url'],
                'expiry_date': user_['expiry_date'],
            }
            record = OSFormRecordsSerializer(data=data)
            if record.is_valid():
                record.save()
            else:
                logging.error(
                    'Submission {form_id} not saved for form {form_name}.'
                    .format(form_id=entry['id'], form_name=form_name)
                )
                user_objs = Contributor.objects.filter(login=user_['user'])
                if user_objs:
                    prepare_and_send_email(record, user_objs.first())
            NETLIFY.delete_submission(entry['id'])


def prepare_and_send_email(record, user):
    """
    Get the User email and the email message body
    :param record: The OSForm Record Serializer object
    :param user: The user object
    :return: The email sent or not
    """
    message = prepare_email_body(record)
    user_email = get_email(user.login)
    email_subject = (
        '{org_name}: Your OpenSource form could not be uploaded'
        .format(org_name=ORG_NAME)
    )
    return send_email(email_subject, user_email, message)


def prepare_email_body(record):
    """
    Prepare the email message body
    :param record: The OSForm Record Serializer object
    :return: the email message body
    """
    message = 'Following problems occurred during adding your form entry-\n'
    for field, error in record.errors.items():
        message += '{key}: {value}\n'.format(key=field, value=' '.join(error))
    return message


def get_email(username):
    """
    Get Publicly visible email of the contributor
    :param username: The GitHub Username
    :return: The user email, if valid username
    """
    user = GitHubUser(token=GITHUB_TOKEN, username=username)
    try:
        user_info = GitHub.get(token=GITHUB_TOKEN, url=user.url)
        return user_info['email']
    except RuntimeError:
        return None
