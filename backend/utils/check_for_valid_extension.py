from coala_utils.Extensions import exts


def check_for_valid_extension(extension):
    for ext in exts:
        if list(exts[ext])[0].lower() == extension.lower():
            return True, ext

    return False, None
