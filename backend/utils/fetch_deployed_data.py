import os
import logging

import requests

from org.git import get_deploy_url, get_upstream_deploy_url


def get_data(repo_name, hoster, filenames, output_dir, allow_failure):
    logger = logging.getLogger(__name__)

    deploy_url = get_deploy_url(repo_name, hoster)
    try:
        upstream_deploy_url = get_upstream_deploy_url(repo_name, hoster)
    except RuntimeError as e:
        upstream_deploy_url = None
        logger.info(str(e))

    for filename in filenames:
        r = requests.get(deploy_url + '/' + filename)
        try:
            r.raise_for_status()
        except Exception:
            if upstream_deploy_url:
                r = requests.get(upstream_deploy_url + '/' + filename)
                if not allow_failure:
                    r.raise_for_status()
            else:
                if allow_failure:
                    return
                else:
                    raise

        filename = os.path.basename(filename)
        with open(os.path.join(output_dir, filename), 'wb+') as f:
            f.write(r.content)
